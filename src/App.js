import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-flexbox-grid';

import './App.css';
import picture from './picture.jpg';
import shapeCopy from './shape-copy.svg';
import shapeCopy2 from './shape-copy-2.svg';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      items: [],
      width: window.innerWidth,
    };

    this.fetchData = this.fetchData.bind(this);
    this.handleWindowSizeChange = this.handleWindowSizeChange.bind(this);
  }

  componentWillMount() {
    this.fetchData();
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange() {
    this.setState({ width: window.innerWidth });
  }

  getDeviceMultiplier() {
    const { width } = this.state;

    if (width >= 992) return 3;
    if (width >= 768) return 2;
    return 1;
  }

  hasMore() {
    const { pagination } = this.state;

    if (!pagination) return true;

    return (pagination.limit + pagination.offset) < pagination.total;
  }

  fetchData() {
    const { items, pagination } = this.state;
    const offset = pagination ? (pagination.offset + pagination.limit) : 0;
    const limit = this.getDeviceMultiplier() * 5;
    const url = `https://api.jqestate.ru/v1/complexes?pagination[limit]=${limit}&pagination[offset]=${offset}`;

    return fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log('responseJson: ', responseJson);
        this.setState({
          items: items.concat(responseJson.items),
          pagination: responseJson.pagination,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  renderItem({ id, name, statistics, images }) {
    const {
      totalPrimaryArea: { from: areaFrom, to: areaTo },
      price: { from: priceFrom },
    } = statistics;

    let pictureSrc;
    if (images.length) pictureSrc = `https://images.jqestate.ru/${images[0].id}-jqestate-512`;
    else pictureSrc = picture;

    return (
      <Col className="item" key={id} xs={12} sm={6} md={4}>
        <Row>
          <img className="picture" alt="" src={pictureSrc} />
        </Row>
        <Row>
          <h5>{`${name}, ${areaFrom.toFixed()} км`}</h5>
        </Row>
        <Row>
          <h2>${priceFrom.usd.toLocaleString()}</h2>
        </Row>
        <Row>
          <span><img className="icon" alt="" src={shapeCopy} />{areaFrom.toFixed()} сот.</span>
          <span><img className="icon" alt="" src={shapeCopy2} />{areaTo.toFixed()} м²</span>
        </Row>
      </Col>
    );
  }

  render() {
    const { items, pagination } = this.state;

    return (
      <Grid>
        <Row>
          {
            items.map(item =>
              this.renderItem(item),
            )
          }
        </Row>
        <Row center="xs" className="action">
          {
            this.hasMore() ?
              <button onClick={this.fetchData}>Загрузить ещё</button>
            : null
          }
        </Row>
      </Grid>
    );
  }
}

export default App;
